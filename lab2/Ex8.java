package lab2;

import java.util.Calendar;
import java.util.Set;

public class Ex8 {
    public static void main(String[] args) {
        var calendar = Calendar.getInstance();
        var freeDays = Set.of(Calendar.SUNDAY, Calendar.SATURDAY);
        var holidays = Set.of(1, 11);

        for (var i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            calendar.set(2018, Calendar.NOVEMBER, i);

            if (freeDays.contains(calendar.get(Calendar.DAY_OF_WEEK))
                    || holidays.contains(calendar.get(Calendar.DAY_OF_MONTH))) {
                System.out.println(String.format("%d Listopad - Wolny", i));
            } else {
                System.out.println(String.format("%d Listopad - Pracujący", i));
            }
        }
    }
}