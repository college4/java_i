package lab2;

public class Ex9 {
    public static void main(String[] args) {
        for (var i = 1; i <= 10; i++) {
            for (var j = 1; j <= 10; j++) {
                System.out.print(String.format("%d ", i * j));
            }
            System.out.println();
        }
    }
}
