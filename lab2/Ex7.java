package lab2;

public class Ex7 {
    public static void main(String[] args) {
        String[] fizzBuzz = { "FizzBuzz", "%d", "%d", "Fizz", "%d", "Buzz", "Fizz", "%d", "%d", "Fizz", "Buzz", "%d",
                "Fizz", "%d", "%d" };

        for (var i = 1; i <= 100; i++) {
            System.out.println(String.format(fizzBuzz[i % 15], i));
        }
    }
}
