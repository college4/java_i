```mermaid
classDiagram
    Car <|-- ElectricCar
    Car <|-- PetrolCar
    PetrolCar <|-- LpgCar
    PetrolCar <|-- TurbochargedPetrolCar

    class Car {
        <<abstract>>
        horsePower: int
        startEngine() void
    }

    class ElectricCar {
        killoWatts: int
        startEngine() void
    }

    class PetrolCar {
        maxPetrolLiters: int
        startEngine() void
        refuel() void
    }

    class LpgCar {
        maxLpgLiters: int
        refuel() void
        changeToLpg() void
    }

    class TurbochargedPetrolCar {
        numberOfTurbocharges: int
        turbocharge() void
    }
```