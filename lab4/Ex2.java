package lab4;

public abstract class Car {
    protected int horsePower;

    public void startEngine() {
        System.out.println("Iu Iu...");
    }
}

public class ElectricCar extends Car {
    public int killoWatts;

    @Override
    public void startEngine() {
        System.out.println("Wsiuuu...");
    }
}

public class PetrolCar extends Car {
    public int maxPetrolLiters;

    @Override
    public void startEngine() {
        System.out.println("Bram bram bram...");
    }

    public void refuel() {
        System.out.println("Gul gul gul...");
    }
}

public class TurbochargedPetrolCar extends PetrolCar {
    public int numberOfTurbocharges;

    public void turbocharge() {
        System.out.println("Faster bram bram bram...");
    }
}

public class LpgCar extends PetrolCar {
    public int maxLpgLiters;

    @Override
    public void refuel() {
        System.out.println("Wsiu wsiu (do not explode...)...");
    }

    public void changeToLpg() {
        System.out.println("I'm cheaper...");
    }
}