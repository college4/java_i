package lab4.Ex3;

public class Kobieta extends Osoba {
    public Kobieta(String imie) {
        super(imie);
    }

    @Override
    public void przedstawSie() {
        System.out.println("Witaj, jestem kobietą, mam na imie " + this.imie);
    }
}