package lab5;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        String pesel;
        Scanner odczyt = new Scanner(System.in); // obiekt do odebrania danych od
        // użytkownika
        System.out.print("Podaj PESEL: ");
        pesel = odczyt.nextLine();

        if (pesel.length() != 11) {
            System.out.println("Pesel ma miec 11 znakow");
            return;
        }

        System.out.println(String.format("%s.%s.%s", pesel.substring(4, 6), pesel.substring(2, 4),
                "19".concat(pesel.substring(0, 2))));
        System.out.println(String.format("%s/%s/%s", pesel.substring(2, 4), pesel.substring(4, 6),
                "19".concat(pesel.substring(0, 2))));
    }

}
