package lab5;

import java.util.Arrays;
import java.util.Comparator;

public class Ex1 {
    public static void main(String[] args) {
        String[] arr = { "  Ala ma kota   ", "a moze nie", "a co jesli tak?" };

        Arrays.asList(arr).stream().map(String::trim).sorted(Comparator.comparingInt(String::length).reversed())
                .forEach(System.out::println);

    }
}
