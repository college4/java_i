package lab5;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex5 {
    public static void main(String[] args) {
        String[] imiona = new String[5];
        Scanner odczyt = new Scanner(System.in); // obiekt do odebrania danych od
        // użytkownika

        for (int i = 0; i < imiona.length; i++) {
            System.out.println("Podaj imie: ");
            imiona[i] = odczyt.nextLine();
        }

        Supplier<Stream<String>> streamSupplier = () -> Arrays.asList(imiona).stream();
        Supplier<Stream<String>> womenSupplier = () -> streamSupplier.get()
                .filter(imie -> imie.charAt(imie.length() - 1) == 'a');
        Supplier<Stream<String>> menSupplier = () -> streamSupplier.get()
                .filter(imie -> imie.charAt(imie.length() - 1) != 'a');

        var womenString = womenSupplier.get().collect(Collectors.joining(", "));
        var manString = menSupplier.get().collect(Collectors.joining(", "));

        System.out.println(String.format("%d kobiety: %s. %d mężczyzn: %s.", womenSupplier.get().count(), womenString,
                menSupplier.get().count(), manString));
    }
}
