package lab5;

import java.util.stream.IntStream;

public class Ex2 {
    public static void main(String[] args) {
        String[] arr = { "stołek", "wtorek" };

        IntStream.range(0, arr.length - 1).forEach(idx -> {
            var firstEl = arr[idx];
            var firstHalf = firstEl.substring(0, firstEl.length() / 2);

            var secondEl = arr[idx + 1];
            var secondElLength = secondEl.length();
            var secondHalf = secondEl.substring(secondElLength / 2, secondElLength);

            System.out.println(firstHalf + secondHalf);
        });

    }
}
