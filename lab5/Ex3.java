package lab5;

public class Ex3 {
    public static void main(String[] args) {
        String plainText = "kolek";
        var initial = "Zaszyfrowany tekst to: ";
        var szyfr = "3wp13";

        var zaszyfrowany = new StringBuffer();

        for (int i = 0; i < plainText.length(); i++) {
            zaszyfrowany.append(szyfr.charAt(i));
        }

        System.out.println(initial.replace(": ", ": ".concat(zaszyfrowany.toString())));

    }

}
