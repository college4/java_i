package lab6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex3 {
    static Random rnd = new Random();

    public static void main(String[] args) {
        List<Integer> listNumbers = generateRandomNumbers();

        listNumbers.forEach(val -> {
            System.out.println(String.format("%d, ", val));
        });

        showMinMax(listNumbers);

        Supplier<Stream<Integer>> streamSupplier = () -> listNumbers.stream();

        var odd = streamSupplier.get().filter(val -> val % 2 != 0).sorted();
        var even = streamSupplier.get().filter(val -> val % 2 == 0).sorted();

        var combined = Stream.concat(even, odd).collect(Collectors.toList());

        combined.forEach(val -> {
            System.out.println(String.format("%d, ", val));
        });

        showMinMax(combined);

    }

    private static List<Integer> generateRandomNumbers() {
        List<Integer> lista = new ArrayList<Integer>();
        int i = 0;
        while (i < 10) {
            lista.add(rnd.nextInt(10));
            i++;
        }
        return lista;
    }

    private static void showMinMax(List<Integer> list) {
        System.out.println(String.format("Najmiejszy: %d", list.indexOf(Collections.min(list))));
        System.out.println(String.format("Największy: %d", list.indexOf(Collections.max(list))));
    }

}
