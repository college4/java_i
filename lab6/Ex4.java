package lab6;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ex4 {
    public static void main(String[] args) {
        List<String> months = List.of("styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień",
                "wrzesień", "październik", "listopad", "grudzień");

        Map<String, Integer> statistics = new HashMap<>();

        months.forEach(month -> Arrays.asList(month.split("")).forEach(letter -> {
            if (statistics.containsKey(letter)) {
                statistics.replace(letter, statistics.get(letter) + 1);

                return;
            }

            statistics.put(letter, 1);

        }));

        statistics.forEach((letter, count) -> {
            System.out.println(String.format("%s - %d", letter, count));
        });

        System.out.println(String.format("Zawiera ć: %s", statistics.containsKey("ć")));
        System.out.println(String.format("Istnieje litera występująca 13 razy? %s", statistics.containsValue(13)));

    }
}
