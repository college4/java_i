package lab6;

import java.util.Collection;
import java.util.List;

public class Ex1 {

    public static void main(String[] args) {
        Collection<Integer> liczby = List.of(1, 2, 21, 13, 8, 7, 3, 2, 45, 23, 8);

        liczby.forEach(val -> {
            System.out.println(String.format("%d - %s", val, val % 2 == 0));
        });
    }

}
