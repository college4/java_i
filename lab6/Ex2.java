package lab6;

import java.util.HashSet;
import java.util.Set;

class Tag {
    private String nazwa;

    public Tag(String nazwa) {
        this.nazwa = nazwa;
    }

    public boolean equals(Object obj) {
        return ((Tag) obj).nazwa.length() == this.nazwa.length();
    }

    public int hashCode() {
        return this.nazwa.length();
    }
}

public class Ex2 {

    public static void main(String[] args) {
        Set<Tag> tagi = new HashSet<Tag>();
        tagi.add(new Tag("nowy"));
        tagi.add(new Tag("nowy"));
        tagi.add(new Tag("nowy"));
        tagi.add(new Tag("przeczytane"));
        if (tagi.size() == 2) {
            System.out.println("Dobra implementacja");
        } else {
            System.out.println("Sprawdź jeszcze raz implementację");
        }
    }

}
