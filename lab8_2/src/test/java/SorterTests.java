import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class SorterTests {
    Sorter sut;
    int[] unsortedNumbers;
    int[] sortedNumbers;


    @Before
    public void Initialize() {
        sut = new Sorter();

        unsortedNumbers = new int[] {1, 5 , 3, 10, 9};
        sortedNumbers = new int[] {1, 3, 5, 9, 10};
    }

    @Test
    public void bubbleSortTest_Should_Sort() {
        sut.bubbleSort(unsortedNumbers);

        Assert.assertArrayEquals(sortedNumbers, unsortedNumbers);
    }

    @Test
    public void insertionSort_Should_Sort() {
        sut.insertionSort(unsortedNumbers);

        Assert.assertArrayEquals(sortedNumbers, unsortedNumbers);
    }

    @Test
    public void getData_Should_Throw_MyException_When_Data_Is_Bad() {
        String input = "5 2 foo";
        InputStream in = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(in);


        Assert.assertThrows(MyException.class, () -> sut.getData());
    }

    @Test
    public void getData_Should_Return_Provided_Data_When_Data_Is_Good() throws MyException {
        String input = "5 2 1";
        InputStream in = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(in);


        Assert.assertArrayEquals(new int[] {5, 2, 1}, sut.getData());
    }
}
