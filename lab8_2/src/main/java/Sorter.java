import java.util.Scanner;

public class Sorter {
    public int[] getData() throws MyException {
        System.out.println("Wprowadz liste liczb:");
        Scanner in = new Scanner(System.in);
        String[] data = in.nextLine().split(" ");
        int[] numbers = new int[data.length];

        for (int i = 0; i < data.length; i++) {
            try {
                numbers[i] = Integer.parseInt(data[i]);
            } catch (Exception ex) {
                throw new MyException();
            }
        }

        in.close();

        return numbers;
    }

    public void bubbleSort(int[] a) {
        boolean sorted = false;
        int temp;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                    sorted = false;
                }
            }
        }
    }

    public void insertionSort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            int current = a[i];
            int j = i - 1;
            while (j >= 0 && current < a[j]) {
                a[j + 1] = a[j];
                j--;
            }

            a[j + 1] = current;
        }
    }
}
