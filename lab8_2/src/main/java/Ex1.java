import java.util.Arrays;
import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) throws MyException {
        Scanner in = new Scanner(System.in);
        Sorter sorter = new Sorter();

        int[] numbers = sorter.getData();

        System.out.println("Wybierz algorymt:\n1) Bubble \n2) Insertion");

        if (Integer.parseInt(in.nextLine()) == 1) {
            sorter.bubbleSort(numbers);
        } else {
            sorter.insertionSort(numbers);
        }

        System.out.println(Arrays.toString(numbers));

        in.close();
    }
}
