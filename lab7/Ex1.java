package lab7;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Ex1 {
    public static void main(String[] args) {
        // 1.
        // metoda(null);
        // Exception in thread "main" java.lang.NullPointerException
        // at lab7.Ex1.metoda(Ex1.java:9)
        // at lab7.Ex1.main(Ex1.java:5)

        // 2.
        // try {
        // metoda(null);
        // } catch (Exception ex) {
        // StringWriter sw = new StringWriter();
        // ex.printStackTrace(new PrintWriter(sw));
        // System.out.println(sw.toString());
        // }
        // java.lang.NullPointerException
        // at lab7.Ex1.metoda(Ex1.java:27)
        // at lab7.Ex1.main(Ex1.java:16)

        // 3.
        // try {
        // metoda(null);
        // } catch (Exception ex) {
        // StringWriter sw = new StringWriter();
        // ex.printStackTrace(new PrintWriter(sw));
        // System.out.println(sw.toString());
        // throw ex;
        // }
        // java.lang.NullPointerException
        // at lab7.Ex1.metoda(Ex1.java:43)
        // at lab7.Ex1.main(Ex1.java:28)

        // Exception in thread "main" java.lang.NullPointerException
        // at lab7.Ex1.metoda(Ex1.java:43)
        // at lab7.Ex1.main(Ex1.java:28)

        // 4.
        // Nie.

        // 5.
        // try {
        // metoda(null);
        // } catch (Exception ex) {
        // StringWriter sw = new StringWriter();
        // ex.printStackTrace(new PrintWriter(sw));
        // System.out.println(sw.toString());
        // throw new Exception();
        // }
        // Exception in thread "main" java.lang.Error: Unresolved compilation problem:
        // Unhandled exception type Exception
        // at lab7.Ex1.main(Ex1.java:53)

        // 6.
        // try {
        // metoda(null);
        // } catch (Exception ex) {
        // StringWriter sw = new StringWriter();
        // ex.printStackTrace(new PrintWriter(sw));
        // System.out.println(sw.toString());
        // throw new Exception(ex);
        // }
        // Exception in thread "main" java.lang.Error: Unresolved compilation problem:
        // Unhandled exception type Exception
        // at lab7.Ex1.main(Ex1.java:53)

    }

    public static void metoda(String napis) {
        System.out.println(napis.length());
    }

}
