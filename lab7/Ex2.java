package lab7;

public class Ex2 {
    void możeZgłosićWyjątek() throws Exception {
        if (new java.util.Random().nextInt(1) == 0)
            throw new Exception();
    }

    public static void main(String[] args) {
        Ex2 z = new Ex2();
        try {
            z.możeZgłosićWyjątek();
            z.możeZgłosićWyjątek();
            z.możeZgłosićWyjątek();
            z.możeZgłosićWyjątek();
            z.możeZgłosićWyjątek();
        } catch (Exception e) {
            System.out.println(e.getStackTrace()[1].getLineNumber());
        }
    }
}
